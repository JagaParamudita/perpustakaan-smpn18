<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\User;
use App\Models\Category;
use Facade\FlareClient\Stacktrace\Stacktrace;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        User::factory(20)->create();

        // User::create([
        //     'name' => 'Muhammad Arfan',
        //     'email' => 'arfan@gmail.com',
        //     'password' => bcrypt('12345')
        // ]);
        // User::create([
        //     'name' => 'Jaga Paramudita',
        //     'email' => 'jaga@gmail.com',
        //     'password' => bcrypt('11111')
        // ]);

        Category::create([
            'name' => 'Programming',
            'slug' => 'programming'
        ]);
        Category::create([
            'name' => 'Photography',
            'slug' => 'photography'
        ]);
        Category::create([
            'name' => 'Design',
            'slug' => 'design'
        ]);
        Category::create([
            'name' => 'Economy',
            'slug' => 'economy'
        ]);
        Category::create([
            'name' => 'Fashion',
            'slug' => 'fashion'
        ]);
        Category::create([
            'name' => 'Covid',
            'slug' => 'covid'
        ]);

        Post::factory()->count(50)->create();
        // Post::create([
        //     'title' => 'PHP One',
        //     'slug' => 'php-one',
        //     'excerpt' => self::$excerpt,
        //     'body' => self::$body,
        //     'category_id' => '1',
        //     "user_id" => '1'
        // ]);
        // Post::create([
        //     'title' => 'PHP Two',
        //     'slug' => 'php-two',
        //     'excerpt' => self::$excerpt,
        //     'body' => self::$body,
        //     'category_id' => '1',
        //     "user_id" => '1'
        // ]);
        // Post::create([
        //     'title' => 'JS One',
        //     'slug' => 'js-one',
        //     'excerpt' => self::$excerpt,
        //     'body' => self::$body,
        //     'category_id' => '3',
        //     "user_id" => '1'
        // ]);
        // Post::create([
        //     'title' => 'Java One',
        //     'slug' => 'java-one',
        //     'excerpt' => self::$excerpt,
        //     'body' => self::$body,
        //     'category_id' => '2',
        //     "user_id" => '2'
        // ]);
    }
}
