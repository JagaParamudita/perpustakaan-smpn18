@extends('layouts/main')


@section('bodyContent')

<h1>


    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Hugo 0.84.0">
        <title>Blog Template · Bootstrap v5.0</title>

        <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/blog/">



        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="aset/vendor/fontawesome-free/css/all.min.css">
        <link href="https://getbootstrap.com/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">


        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }

            .kartu {
                width: 800px;
                margin: 0 auto;
                margin-top: 70px;
                box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, .03);
                transition: all .3s;
            }

            .foto {
                padding: 20px;
            }

            tbody {
                font-size: 20px;
                font-weight: 300;
            }

            .biodata {
                margin-top: 30px;
            }
        </style>


        <!-- Custom styles for this template -->
        <link href="https://fonts.googleapis.com/css?family=Playfair&#43;Display:700,900&amp;display=swap" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="blog.css" rel="stylesheet">
    </head>

    <body>

        <section class="card shadow-lg mt-4">
            <div>
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators center">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="assets/img/bg.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="assets/img/bg.jpg" class="d-block w-100" alt="...">
                        </div>

                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </section>


        <section>
            <div class="container mt-2">
                <div class="row">
                    <div class="col-md-7">
                        <div class="">
                            <h1> Kata Sambutan</h1>
                        </div>
                        <div style="color: rgb(118, 157, 29)">
                            <h2>Assalamualaikum wr. wb.</h2>
                        </div>

                        <div style="font-size: 20px; ">
                            <p>Syukur alhamdulillah kami panjatkan kepada Alloh Tuhan Yang Maha Esa, disertai ucapan
                                terima kasih kepada Tim IT UPTD SPF SMP Negeri 18 Tegal yang telah mampu mengaktifkan
                                kembali Web Sekolah diiringi perasaan lega karena apa yang diharapkan oleh warga sekolah
                                untuk menyediakan akses dan berbagai informasi guna mengembangkan dan memperlancar
                                semua sistem yang ada di sekolah bisa terfasilitasi terutama dalam mengembangkan kegiatan
                                pembelajaran.
                            </p>
                        </div>
                        <div style="font-size: 20px; ">
                            <p>Kemajuan Teknologi Informasi dapat dimanfaatkan untuk proses pembelajaran lebih inovatif,
                                untuk penyebarluasan informasi dari semua kegiatan pendidikan yang dilakukan di sekolah,
                                sehingga yang membutuhkan informasi termasuk stake holder dapat terpenuhi. Sebagai media
                                pembelajaran, Website sekolah dapat memuat blog guru di dalamnya dapat dituliskan berbagai
                                artikel pembelajaran atau materi penting, bahkan dapat memberikan tugas mandiri kepada
                                peserta didik. Seperti inilah pembelajaran yang berbasis Teknologi Informasi.
                            </p>
                        </div>
                        <div style="font-size: 20px; ">
                            <p>Website sebagai sarana promosi sekolah sangat efektif, berbagai kegiatan dan prestasi sekolah
                                dapat diunggah, disertai gambar-gambar yang relevan, masyarakat dapat mengetahui prestasiprestasi yang telah berhasil diraih oleh UPTD SPF SMP Negeri 18 Tegal.. Website dapat
                                dijadikan media komunikasi dan konsulidasi antara sekolah dengan para alumni. Dengan
                                harapan terbentuk ikatan alumni yang makin besar dan kuat.

                            </p>
                        </div>
                        <div style="font-size: 20px; ">
                            <p>Masih banyak kekurangan dari Web UPTD SPF SMP Negeri 18 Tegal, oleh karenanya, kami
                                akan terus meng-up date diri, sehingga isi dan mutunya akan selalu lebih baik. Terima kasih dan
                                selamat pada Tim IT maju terus untuk kemajuan UPTD SPF SMP Negeri 18 Tegal menjadi lebih
                                pintar, cerdas, dan berbudaya, serta berakhlakul karimah. Aamiin
                            </p>
                        </div>
                        <div class="mb-4">
                            <h2>Wassalamualaikum wr. wb.</h2>
                        </div>


                    </div>
                    <div class="col-md-5">
                        <img src="assets/poto.png" class="img-fluid mb-3" alt="#">
                    </div>
                </div>
            </div>
        </section>

        <!-- image slider -->



        <div class="container">
            <div class="card shadow mt-4 ">
                <div class="row mb-4 m-4">

                    <div class="col-md-8 kertas-biodata">
                        <div class="biodata">
                            <table width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td valign="top">
                                            <table border="0" width="100%" style="padding-left: 2px; padding-right: 13px;">
                                                <tbody>
                                                    <h1 class="mb-4">Identitas Sekolah</h1>
                                                    <tr>
                                                        <td valign="top" class="textt">Nama Sekolah </td>
                                                        <td width="2%">:</td>
                                                        <td style="color: rgb(118, 157, 29); font-weight:bold">UPTD SMP 18 TEGAL</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="textt">Alamat</td>
                                                        <td>:</td>
                                                        <td>Jl. Abdul Syukur No.45A Kec. Margana Kota Tegal
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="textt">No. Telepon </td>
                                                        <td>:</td>
                                                        <td>(0283) 340821</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="textt">NPSN</td>
                                                        <td>:</td>
                                                        <td>20329835</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="textt">Status Sekolah</td>
                                                        <td>:</td>
                                                        <td>Negeri</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="textt">SK Pendiirian Sekolah</td>
                                                        <td>:</td>
                                                        <td>107/0/1997</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="textt">Tanggal SK Pendirian</td>
                                                        <td>:</td>
                                                        <td>16-05-1997</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="textt">Kepemilikan Tanah / Bangunan</td>
                                                        <td>:</td>
                                                        <td>Milik Pemerintah</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="textt">Luas Tanah / Status</td>
                                                        <td>:</td>
                                                        <td>17.230 m² / Hak Pakai</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="textt">Luas Bangunan</td>
                                                        <td>:</td>
                                                        <td>2198 m2</td>
                                                    </tr>
                                                    <tr>

                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- footer -->
        <footer>
            <a style="font-size:10px" href="#">up</a>
        </footer>





    </body>

    </html>


</h1>

@endsection