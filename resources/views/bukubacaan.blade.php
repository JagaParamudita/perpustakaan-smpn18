@extends('layouts/main')


@section('bodyContent')

<h1>

    <!doctype html>
    <html lang="en">

    <head>


        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
        <title>Katalog Buku Bacaan</title>
    </head>





    <header class="d-md-none">

        <nav class="bg-dark fixed-top py-3 row">
            <div class="col-6 ps-4 "><a class="text-decoration-none fw-bold fs-4 text-white" data-bs-toggle="offcanvas" role="button" href="#offcanvasWithBothOptions" aria-controls="offcanvasWithBothOptions">☰ E-Library</a></div>
            <!-- <div class="col-2 ps-4 showWhenScroll"><a class="text-decoration-none fw-bold fs-4 text-white" data-bs-toggle="offcanvas" role="button" href="#offcanvasWithBothOptions" aria-controls="offcanvasWithBothOptions">☰</a></div> -->
            <div class="offset-3 col-3 pe-4">
                <ul class="text-end list-unstyled navbar-nav" style="--bs-scroll-height: 100px;">
                    <li class="nav-item dropdown ">
                        <a class="nav-link dropdown-toggle text-light fw-light fs-5 p-0" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <img class="rounded-circle" width="30" height="30" src="http://localhost:8080/E-Library/Public/img/profilePicture/jagaparamudita_233335396.jpg" alt="">
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarScrollingDropdown">
                            <li><a class="ps-2 dropdown-item" href="http://localhost:8080/E-Library/Public/user/profile_settings">Profile</a></li>
                            <li><a class="ps-2 dropdown-item" href="http://localhost:8080/E-Library/Public/user/security_settings">Setting</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="ps-2 dropdown-item" href="http://localhost:8080/E-Library/Public/user/signOut">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <form action="" class="showWhenScroll col-12 px-4 pt-2 pb-0">
                <div class="input-group ">
                    <input type="text" class="form-control" placeholder="Search e-book, etc." aria-describedby="button-addon2">
                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">Search</button>
                </div>
            </form>
        </nav>








        <div class="offcanvas offcanvas-start w-75" data-bs-scroll="true" tabindex="-1" id="offcanvasWithBothOptions" aria-labelledby="offcanvasWithBothOptionsLabel">
            <div class="offcanvas-header pb-1 border-bottom shadow-sm border-dark bg-dark">
                <div class="offcanvas-title" id="offcanvasWithBothOptionsLabel"><a href="#" class="fw-bold d-flex fs-5 align-items-center mb-2 mb-lg-0 text-white text-decoration-none fs-4 me-2">
                        <svg class="me-1" xmlns="http://www.w3.org/2000/svg" width="36" height="36" fill="currentColor" class="bi bi-book" viewBox="0 0 16 16">
                            <path d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811V2.828zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492V2.687zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                        </svg>E-Library
                    </a></div>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <div class="mb-1">
                    <a class="text-decoration-none text-dark fs-4" href="http://localhost:8080/E-Library/Public/">Home</a>
                </div>
                <div class="mb-1">
                    <a class="text-decoration-none text-dark fs-4" href="http://localhost:8080/E-Library/Public/">Category</a>
                </div>
            </div>
        </div>
    </header>



    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script>
        let noneWhenScroll = $('.noneWhenScroll');
        let searchNavbar = $('.searchNavbar');
        let showWhenScroll = $('.showWhenScroll');
        let tempBlank = $('.tempBlank');
        document.addEventListener("DOMContentLoaded", function() {
            tempBlank.fadeOut();
        });
        // window.addEventListener("load", function() {
        //   tempBlank.fadeOut();
        // });
        $(document).ready(function() {
            noneWhenScroll.addClass('d-none');
            showWhenScroll.addClass('d-none');
            searchNavbar.addClass('d-none');
            $(this).scrollTop() > 500 ? showWhenScroll.removeClass('d-none') : noneWhenScroll.removeClass('d-none');
        });
        $(document).scroll(function() {
            let y = $(this).scrollTop();
            if (y > 500) {
                noneWhenScroll.addClass('d-none');
                showWhenScroll.removeClass('d-none');
                searchNavbar.removeClass('d-none');
                searchNavbar.fadeIn();
            } else {
                noneWhenScroll.removeClass('d-none');
                showWhenScroll.addClass('d-none');
                searchNavbar.addClass('d-none');
                // $('.searchNavbar').fadeOut();
            }
        });
    </script>

    <div class="row ">
        <div class="col-md-4 col-12">
            <h4 class="fs-4 text-dark font-monospace">Katalog : Buku Bacaan </h4>
        </div>

        <form class="col-md-8 col-12 mb-2 mb-md-1" action="http://localhost:8080/E-Library/Public/collection/search" method="POST">
            <div class="input-group mb-3">
                <a class="btn btn-light " href="http://localhost:8080/E-Library/Public/collection"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M13.5 2c-5.288 0-9.649 3.914-10.377 9h-3.123l4 5.917 4-5.917h-2.847c.711-3.972 4.174-7 8.347-7 4.687 0 8.5 3.813 8.5 8.5s-3.813 8.5-8.5 8.5c-3.015 0-5.662-1.583-7.171-3.957l-1.2 1.775c1.916 2.536 4.948 4.182 8.371 4.182 5.797 0 10.5-4.702 10.5-10.5s-4.703-10.5-10.5-10.5z" />
                    </svg></a>
                <input required value="" name="searchKey" type="text" class="form-control" placeholder="Search e-book, etc.">
                <button class="btn btn-secondary" type="submit" id="button-addon2">Search</button>
            </div>
        </form>

        <!-- Baris Ke 1 -->
        <!-- BUKU1 -->
        <div class="col-md-4 d-none d-md-flex">
            <div class="card bg-light mb-1" style="width: 540px;">
                <div class="row g-0">
                    <div class="col-md-4" style="background-image: url('http://books.google.com/books/content?id=GAqBAwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api'); object-fit : cover ; background-position : center ;">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Bahasa Indonesgggia</h5>
                            <p class="fontcard">Semester II</p>
                            <a href="https://drive.google.com/file/d/16mQx-I9iE34ZlY0o1uHMHC4dw7l8lOHa/view?usp=drivesdk"> <button class="btn btn-dark">Baca sekarang</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- BUKU1 -->

        <!-- BUKU2 -->

        <div class="col-md-4 d-none d-md-flex">
            <div class="card bg-light mb-1" style="width: 540px;">
                <div class="row g-0">
                    <div class="col-md-4" style="background-image: url('http://books.google.com/books/content?id=GAqBAwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api'); object-fit : cover ; background-position : center ;">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Bahasa Indonesgggia</h5>
                            <p class="fontcard">Semester II</p>
                            <a href="https://drive.google.com/file/d/16mQx-I9iE34ZlY0o1uHMHC4dw7l8lOHa/view?usp=drivesdk"> <button class="btn btn-dark">Baca sekarang</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- BUKU2 -->

        <!-- BUKU3 -->

        <div class="col-md-4 d-none d-md-flex">
            <div class="card bg-light mb-1" style="width: 540px;">
                <div class="row g-0">
                    <div class="col-md-4" style="background-image: url('http://books.google.com/books/content?id=GAqBAwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api'); object-fit : cover ; background-position : center ;">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Bahasa Indonesgggia</h5>
                            <p class="fontcard">Semester II</p>
                            <a href="https://drive.google.com/file/d/16mQx-I9iE34ZlY0o1uHMHC4dw7l8lOHa/view?usp=drivesdk"> <button class="btn btn-dark">Baca sekarang</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BUKU3 -->
        <!-- Baris Ke 1 -->


        <!-- Baris Ke 2 -->
        <!-- BUKU1 -->
        <div class="col-md-4 d-none d-md-flex">
            <div class="card bg-light mb-1" style="width: 540px;">
                <div class="row g-0">
                    <div class="col-md-4" style="background-image: url('http://books.google.com/books/content?id=GAqBAwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api'); object-fit : cover ; background-position : center ;">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Bahasa Indonesgggia</h5>
                            <p class="fontcard">Semester II</p>
                            <a href="https://drive.google.com/file/d/16mQx-I9iE34ZlY0o1uHMHC4dw7l8lOHa/view?usp=drivesdk"> <button class="btn btn-dark">Baca sekarang</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- BUKU1 -->

        <!-- BUKU2 -->

        <div class="col-md-4 d-none d-md-flex">
            <div class="card bg-light mb-1" style="width: 540px;">
                <div class="row g-0">
                    <div class="col-md-4" style="background-image: url('http://books.google.com/books/content?id=GAqBAwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api'); object-fit : cover ; background-position : center ;">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Bahasa Indonesgggia</h5>
                            <p class="fontcard">Semester II</p>
                            <a href="https://drive.google.com/file/d/16mQx-I9iE34ZlY0o1uHMHC4dw7l8lOHa/view?usp=drivesdk"> <button class="btn btn-dark">Baca sekarang</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- BUKU2 -->

        <!-- BUKU3 -->

        <div class="col-md-4 d-none d-md-flex">
            <div class="card bg-light mb-1" style="width: 540px;">
                <div class="row g-0">
                    <div class="col-md-4" style="background-image: url('http://books.google.com/books/content?id=GAqBAwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api'); object-fit : cover ; background-position : center ;">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Bahasa Indonesgggia</h5>
                            <p class="fontcard">Semester II</p>
                            <a href="https://drive.google.com/file/d/16mQx-I9iE34ZlY0o1uHMHC4dw7l8lOHa/view?usp=drivesdk"> <button class="btn btn-dark">Baca sekarang</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BUKU3 -->
        <!-- Baris Ke 1 -->


        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>


        </body>

    </html>



</h1>

@endsection