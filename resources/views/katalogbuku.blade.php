@extends('layouts/main')


@section('bodyContent')

<h1>

    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Hugo 0.84.0">
        <title>Blog Template · Bootstrap v5.0</title>

        <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/blog/">



        <!-- Bootstrap core CSS -->
        <link href="/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <!-- Favicons -->
        <link rel="apple-touch-icon" href="/docs/5.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
        <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
        <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
        <link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
        <link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
        <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">
        <meta name="theme-color" content="#7952b3">


        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>


        <!-- Custom styles for this template -->
        <link href="https://fonts.googleapis.com/css?family=Playfair&#43;Display:700,900&amp;display=swap" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="blog.css" rel="stylesheet">
    </head>

    <body>

        <!-- Header -->
        <main class="container">
            <div class="p-3 p-sm-5 mb-4 bg-light rounded-3" style="background-image: url('https://images.unsplash.com/photo-1544716278-e513176f20b5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80');">
                <div class="container-fluid py-5">
                    <h1 class="display-5 fw-bold"> Selamat Datang Di Perpustakaan Digital SMP Negeri 18 KOTA TEGAL</h1>
                    <p class="col-md-8 fs-4">Siapapun Kamu, Apapun Cita-citamu, Rajinlah Membaca Buku</p>
                    <!-- <button class="btn btn-primary btn-lg" type="button">Example button</button> -->
                    <div class="input-group mb-3 ">
                        <input type="text" class="form-control form-control-lg" placeholder="Cari e-Buku Sekarang" aria-describedby="button-addon2">
                        <button class="btn btn-dark" type="button" id="button-addon2">Cari</button>
                    </div>
                </div>
            </div>
            <!-- Header -->




            <div class="row">
                <div class="col-sm-12">
                    <div class="card-group">
                        <div class="card" style="width: 18rem;">
                            <img src="assets/img/bind.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Katalog buku kelas 7</h5>
                                <a href="/buku7" class="btn btn-primary">Lihat Selengkapnya</a>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="card" style="width: 18rem;">
                            <img src="assets/img/pkn7.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Katalog buku kelas 8</h5>
                                <a href="/buku8" class="btn btn-primary">Lihat Selengkapnya</a>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="card" style="width: 18rem;">
                            <img src="assets/img/bind.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Katalog buku kelas 9</h5>
                                <a href="/buku9" class="btn btn-primary">Lihat Selengkapnya</a>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="card" style="width: 18rem;">
                            <img src="assets/img/pkn7.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Katalog buku bacaan </h5>
                                <a href="/bukubacaan" class="btn btn-primary">Lihat Selengkapnya</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>









            <div class="row g-5">
                <div class="col-md-8">
                    <h3 class="pb-4 mb-4 fst-italic border-bottom">
                        Informasi mengenai buku
                    </h3>

                    <article class="blog-post">
                        <h2 class="blog-post-title">Manfaat membaca buku</h2>


                        <p>Buku adalah jendela dunia, dan kegiatan membaca buku merupakan suatu cara untuk membuka jendela tersebut agar kita bisa mengetahui lebih tentang dunia yang belum kita tahu sebelumnya. Kegiatan tersebut dapat dilakukan oleh siapa saja, anak-anak, remaja, dewasa, maupun orang-orang yang telah berusia lanjut.</p>
                        <hr>
                        <p>Buku merupakan sumber berbagai informasi yang dapat membuka wawasan kita tentang berbagai hal seperti ilmu pengetahuan, ekonomi, sosial, budaya, politik, maupun aspek-aspek kehidupan lainnya. Selain itu, dengan membaca, dapat membantu mengubah masa depan, serta dapat menambah kecerdasan akal dan pikiran kita.</p>
                        <hr>
                        <h3>Example lists</h3>
                        <p>This is some additional paragraph placeholder content. It's a slightly shorter version of the other highly repetitive body text used throughout. This is an example unordered list:</p>
                        <ul>
                            <li>First list item</li>
                            <li>Second list item with a longer description</li>
                            <li>Third list item to close it out</li>
                        </ul>

                        <p>And don't forget about tables in these posts:</p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Katalog Buku</th>
                                    <th>Jumlah Buku</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Buku Kelas 7</td>
                                    <td>10</td>
                                </tr>



                                <tr>
                                    <td>Buku Kelas 8</td>
                                    <td>4</td>

                                </tr>
                                <tr>
                                    <td>Buku Kelas 9</td>
                                    <td>7</td>

                                </tr>

                                <tr>
                                    <td>Buku Bacaan</td>
                                    <td>7</td>

                                </tr>



                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>Totals</td>
                                    <td>21</td>

                                </tr>
                            </tfoot>
                        </table>


                    </article>



                    <nav class="blog-pagination" aria-label="Pagination">
                        <a class="btn btn-outline-primary" href="#">Older</a>
                        <a class="btn btn-outline-secondary disabled" href="#" tabindex="-1" aria-disabled="true">Newer</a>
                    </nav>

                </div>

                <div class="col-md-4">
                    <div class="position-sticky" style="top: 2rem;">
                        <div class="p-4 mb-3 bg-light rounded">
                            <h4 class="fst-italic">About</h4>
                            <p class="mb-0">Customize this section to tell your visitors a little bit about your publication, writers, content, or something else entirely. Totally up to you.</p>
                        </div>




                    </div>
                </div>

        </main>

        <footer class="blog-footer">
            <p>Blog template built for <a href="https://getbootstrap.com/">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
            <p>
                <a href="#">Back to top</a>
            </p>
        </footer>










    </body>

    </html>

</h1>

@endsection