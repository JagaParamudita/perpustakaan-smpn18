<section class="container">
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark ">
        <div class="container">
            <img src="assets/img/logo.png" width="150" href="/">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="btn-outline-success">
                        <a class="nav-link" href="/">HOME</a>
                    </li>
                    <li class="btn-outline-success">
                        <a class="nav-link" href="/katalogbuku">KATALOG BUKU</a>
                    </li>
                    </li>
                    <li class="btn-outline-success">
                        <a class="nav-link" href="/daftarpengunjung">DAFTAR PENGUNJUNG</a>
                    </li>
                    <li class="btn-outline-success">
                        <a class="nav-link" href="/about">ABOUT</a>
                    </li>



                </ul>
                <li class="btn btn-outline-primary">
                    <a class="nav-link" href="/formtamu">DAFTAR</a>
                </li>


            </div>
        </div>
    </nav>
    <div class="py-5"></div>