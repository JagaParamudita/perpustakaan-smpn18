<?php

use App\Http\Controllers\PostController;
use App\Models\Category;
use Illuminate\Support\Facades\Route;
use App\Models\Post;
use App\Models\User;
use App\Http\Controllers\C_Login;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home', [
        'htmlTitle' => 'Perpustakaan | Home'
    ]);
});


Route::get('katalogbuku', function () {
    return view('katalogbuku', [
        'htmlTitle' => 'Perpustakaan | Katalog Buku',

    ]);
});

Route::get('daftarpengunjung', function () {
    return view('daftarpengunjung', [
        'htmlTitle' => 'Perpustakaan | Daftar Pengunjung',

    ]);
});


Route::get('about', function () {
    return view('about', [
        'htmlTitle' => 'Perpustakaan | About',

    ]);
});



Route::get('buku7', function () {
    return view('buku7', [
        'htmlTitle' => 'Katalog Buku | Kelas 7',

    ]);
});

Route::get('buku8', function () {
    return view('buku8', [
        'htmlTitle' => 'Katalog Buku | Kelas 8',

    ]);
});
Route::get('buku9', function () {
    return view('buku9', [
        'htmlTitle' => 'Katalog Buku | Kelas 9',

    ]);
});

Route::get('bukubacaan', function () {
    return view('bukubacaan', [
        'htmlTitle' => 'Katalog Buku | Bacaan',

    ]);
});

Route::get('formtamu', function () {
    return view('formtamu', [
        'htmlTitle' => 'Buku Tamu',

    ]);
});
Route::get('/login', [C_Login::class, 'index'])->name('baliklogin');
Route::get('/login/masuk', [C_Login::class, 'dashboard'])->name('dashboard');
